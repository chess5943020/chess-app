import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-main-app',
  templateUrl: './main-app.component.html',
  styleUrls: ['./main-app.component.css']
})
export class MainAppComponent implements OnInit {
  @ViewChild('iframe1') iframe1: ElementRef;
  @ViewChild('iframe2') iframe2: ElementRef;

  constructor() { }

  ngOnInit(): void {
    // listen for messages from the iframes
    window.addEventListener('message', this.handleCommingMessage.bind(this));
  }

  // handle messages from the iframes
  handleCommingMessage(event: MessageEvent): void {
    console.log("********* parent listens well")
    console.log(event.data);

    // check the origin of the message
    // if (event.origin !== 'https://example.com') {
    //   return;
    // }

    // check the source of the message
    // if (event.source === this.iframe1.nativeElement.contentWindow) {
    //   // message from iframe1, send it to iframe2
    //   this.iframe2.nativeElement.contentWindow.postMessage(event.data, 'https://example.com');
    // } else if (event.source === this.iframe2.nativeElement.contentWindow) {
    //   // message from iframe2, send it to iframe1
    //   this.iframe1.nativeElement.contentWindow.postMessage(event.data, 'https://example.com');
    // }
  }

  sendPostMessageToIFrame(frameId: number) {

    switch (frameId) {
      case 1:
        this.iframe1.nativeElement.contentWindow.postMessage('Hello iframe1 from parent', '*');
        break;

      case 2:
        this.iframe2.nativeElement.contentWindow.postMessage('Hello ifram2 from parent', '*');
        break;
    }
  }

}
