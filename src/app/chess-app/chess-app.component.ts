import { Component, EventEmitter, HostListener, Input, OnInit, Output, ViewChild } from '@angular/core';
import { NgxChessBoardModule, NgxChessBoardService, NgxChessBoardView } from "ngx-chess-board";

@Component({
  selector: 'app-chess-app',
  templateUrl: './chess-app.component.html',
  styleUrls: ['./chess-app.component.css']
})
export class ChessAppComponent implements OnInit {

  currentPlayer: string = 'white';
  currentIsCheck: boolean = false;
  currentEvent: any;

  @ViewChild('board', { static: false }) board: NgxChessBoardView;

  constructor(private ngxChessBoardService: NgxChessBoardService) {
  }

  ngOnInit(): void {
    // check if there is a saved game state in localstorage
    const fen = localStorage.getItem('gameState');
    if (fen) {
    (this.ngxChessBoardService as any).setFEN(fen);
    }

    // listen for messages from the parent window
    window.addEventListener('message', this.handleCommingMessage.bind(this));
  }

  onMoveChange(event: any) {
    console.log("******* onMoveChange is invoked!")
    this.getMoveHistoryData(event);


    // 1- check if isCheck, the show alert and button for new game
    // 2- if not check, send current move to the other iframe
    // 3- send current move to the other iframe

    if (this.isCheck()) {
      // perform 1
    } else {
      // perform 2
      this.changeCurrentPlayer();

      // perform 3
      this.sendPostMessageToParent();
    }
  }

  private isCheck() {
    if (this.currentIsCheck) {
      return true;
    }
    return false;
  }

  private changeCurrentPlayer() {
    if (this.currentPlayer === ' white') {
      // set dark to be enabled: works fine!
      (this.board as any).darkDisabled = false;
    } else {
      // set dark to be enabled: works fine!
      (this.board as any).whiteDisabled = false;
    }
  }

  private getMoveHistoryData(event: any) {
    this.currentPlayer = this.board.getMoveHistory()[this.board.getMoveHistory().length - 1]['color']
    this.currentIsCheck = this.board.getMoveHistory()[this.board.getMoveHistory().length - 1]['check']
    this.currentEvent = event;

    // console.log("getMoveHistory : ", this.board.getMoveHistory())
    //let moveHistory = this.board.getMoveHistory()
    // console.log("ischeck : ", moveHistory[moveHistory.length - 1 ]['check'])
    // console.log("lastMovie : from ", moveHistory[moveHistory.length - 1]['move'].substring(0, 2))
    // console.log("lastMovie : to ", moveHistory[moveHistory.length - 1]['move'].substring(2, 4))
    // console.log("lastMovie player : ", moveHistory[moveHistory.length - 1]['color'])


  }

  private sendPostMessageToParent() {

    // send full event
    window.parent.postMessage(this.currentEvent, '*');
  }

  private handleCommingMessage(event: any) {

    // check the origin of the message
    if (event.origin !== 'https://example.com') {
      return;
    }
    // check the source of the message
    if (event.source === window.parent) {
      // message from parent, do something with it
      console.log(event.data); // Hello from parent
    }
  }

  // store the game state in localstorage when the user closes the browser
  @HostListener('window:beforeunload', ['$event'])
  beforeUnloadHandler(event: any) {
    const fen = (this.ngxChessBoardService as any).getFEN();
    localStorage.setItem('gameState', fen);
  }
}
