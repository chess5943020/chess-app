import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ChessAppComponent } from './chess-app/chess-app.component';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { NgxChessBoardModule } from "ngx-chess-board";
import { MainAppComponent } from './main-app/main-app.component';
import { HomeAppComponent } from './home-app/home-app.component';

const appRoutes: Routes = [
  { path: '', component: HomeAppComponent },
  { path: 'mainpage', component: MainAppComponent },
  { path: 'chess-board', component: ChessAppComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    ChessAppComponent,
    MainAppComponent,
    HomeAppComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(
      appRoutes,
    ),
    NgxChessBoardModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
